<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/sys/functions.php')
?><!DOCTYPE html>
<html lang="de">
<head>
	<meta charset="UTF-8" />
	<title>Klappfunktion mit jQuery</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdn.normanhuth.com/assets/prism/css/prism.min.css">
	<style>
		html {
			max-width: 1000px;
			margin: 0 auto;
			box-sizing: border-box;
		}
		*,
		*:before,
		*:after {
			box-sizing: inherit;
		}
		body {
			position: relative;
			margin: 0;
			padding-top: 2rem;
			padding-bottom: 6rem;
			min-height: 100%;
			background-color: rgba(192,192,192,0.8);
			border: none;
			width: 100%;
		}
		.card-midheader {
			padding: .75rem 1.25rem;
			margin-bottom: 0;
			background-color: rgba(0,0,0,.03);
			border-bottom: 1px solid rgba(0,0,0,.125);
			border-top: 1px solid rgba(0,0,0,.125);
		}
		.nhexpand_content {
			display: none;
		}
	</style>
</head>
<body>
	
		<div class="card">
			<div class="card-header">
				Vorschau 1: Text beim &ouml;ffnen der Seite zugeklappt
			</div>
			<div class="card-body">
				<a href="#" class="nhexpand" id="expand1">Mehr anzeigen</a>
				<div class="nhexpand_content" id="content_expand1">
					Beispiel Text 1 steht hier.
				</div>
			</div>
			<div class="card-midheader">
				Vorschau 2: Text beim &ouml;ffnen der Seite aufgeklappt
			</div>
			<div class="card-body">
				<a href="#" class="nhexpand" id="expand2">Weniger anzeigen</a>
				<div class="alert alert-dark" id="content_expand2">
					Hier steht Beispieltext 2
					<p>Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
			</div>
			<div class="card-midheader">
				Anleitung: Schritt 1
			</div>
			<div class="card-body">
				<p>Folgende CSS-Angabe habe ich f&uuml;r diese Klappfunktion gesetzt</p>
				<p class="text-muted">(das grau hinterlegte aus Vorschau habe ich in dieser Anleitung ausgelassen. Viel Spa&szlig; beim experimentieren)</p>
				<?php NHparsePrism('css','.nhexpand_content {
	display: none;
}'); ?>
				<p>Dazu muss noch die JavaScript-Bibliothek jQuery &amp; die Klapp-Funktion integriert werden</p>
				<?php NHparsePrism('html','<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
	$(document).ready(function(){
		$(\'.nhexpand\').click(function(){
			var id	= $(this).attr(\'id\');
			var txt	= $(this);
			$(\'#content_\'+id).slideToggle(\'fast\',function() {
				if($(this).is(":visible")) {
					txt.text(\'Weniger anzeigen\');                
				} else {
					txt.text(\'Mehr anzeigen\');                
				}
			});
		});
	});
</script>'); ?>
				<p>Am Besten ihr f&uuml;gt alle Javascripte immer am Ende <strong>vor</strong> dem <code>&lt;/body&gt;</code> ein.</p>
				<p>In dem Script findet Ihr die Geschwindigkeitsangabe <code>'fast'</code>, dieser Wert kann auch <code>'slow'</code> sein oder als Ziffer in Millisekunden angegeben werden.</p>
				<p>In der &quot;If-Else&quot;-Funktion befindet sich, der Text den man anpassen kann. Man kann diese Funktion auch anpassen oder entfernen/ausklammern.</p>
			</div>
			<div class="card-midheader">
				Anleitung: Schritt 2
			</div>
			<div class="card-body">
				<p>Hier ist der Quelltext f&uuml;r die 2 Klapptexte.</p>
				<p class="text-muted">Einmal mit dem Text zu- &amp; einmal aufgeklappt beim &ouml;ffnen der Seite.</p>
				<?php NHparsePrism('html','<!-- Beispiel 1 -->
<a href="#" class="nhexpand" id="expand1">Mehr anzeigen</a>
<div class="nhexpand_content" id="content_expand1">
	Beispiel Text 1 steht hier.
</div>
<!-- Beispiel 2 -->
<a href="#" class="nhexpand" id="expand2">Weniger anzeigen</a>
<div id="content_expand2">
	Hier steht Beispieltext 2
	<p>Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
</div>'); ?>
				<p>Das Element <code>id="expand1"</code> &ouml;ffnet das Element <code>id="content_expand1"</code> &amp; das Element <code>id="expand2"</code> &ouml;ffnet das Element <code>id="content_expand2"</code>. Die Ziffern k&ouml;nen aleso f&uuml;r weitere Elemente ausgetauscht werden.</p>
				<p>Die Klasse <code>nhexpand_content</code> im ersten <code>div</code>-Element sorgt daf&uuml;r, dass der Inhalt von diesem Element beim &ouml;ffnen der Seite nicht sichtbar ist.</p>
			</div>
		</div>

	<script src="https://cdn.normanhuth.com/assets/prism/js/prism.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script>
		$(document).ready(function(){
			$('.nhexpand').click(function(){
				var id	= $(this).attr('id');
				var txt	= $(this);
				$('#content_'+id).slideToggle('fast',function() {
					if($(this).is(":visible")) {
						txt.text('Weniger anzeigen');                
					} else {
						txt.text('Mehr anzeigen');                
					}
				});
			});
		});
	</script>
</body>
</html>
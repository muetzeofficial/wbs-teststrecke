<?php

include_once($_SERVER['DOCUMENT_ROOT'].'/sys/functions.php')

?>
<!doctype html>
<html lang="de">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Zitat</title>
	<link rel="stylesheet" href="/index.php?mode=css">
	<link rel="stylesheet" href="https://cdn.normanhuth.com/assets/prism/css/prism.min.css">
	<style>
		body {
			background-color: rgba(51,51,51,0.75);
		}
		html {
			max-width: 1000px;
		}
/* blockquote format START */
		blockquote {
			background-color: snow;
			font-family: 'Monotype Corsiva','Apple Chancery','ITC Zapf Chancery','URW Chancery L',cursive;
			font-size: x-large;
			border-radius: 10px;
			text-align: center;
			padding: 5px;
			margin-left: 5px !important;
			margin-right: 5px !important;
			border-style: dashed;
			margin: 0 auto;
		}
		blockquote p {
			margin-left: 10px;
			margin-right: 10px;
		}
		blockquote p.author {
			color: whitesmoke;
			font-size: large;
		}
/* Gestreifter Hintergrund https://css-tricks.com/stripes-css/ */
		.stripe-2 {
			color: white;
			background: repeating-linear-gradient(
				-55deg,
				#222,
				#222 10px,
				#333 10px,
				#333 20px
			);
		}
/* blockquote format END */
	</style>
</head>
<body>

<!-- blockquote START -->
	<blockquote class="stripe-2">
		<p>Die Erinnerung ist das einzige Paradies,<br>aus dem wir nicht vertrieben werden k&ouml;nnen.</p>
		<p class="author">- Jean Paul (1763 - 1825)</p>
	</blockquote>
	<?php NHparsePrism('css','blockquote {
	background-color: snow;
	font-family: \'Monotype Corsiva\',\'Apple Chancery\',\'ITC Zapf Chancery\',\'URW Chancery L\',cursive;
	font-size: x-large;
	border-radius: 10px;
	text-align: center;
	padding: 5px;
	margin-left: 5px !important;
	margin-right: 5px !important;
	border-style: dashed;
	margin: 0 auto;
}
blockquote p {
	margin-left: 10px;
	margin-right: 10px;
}
blockquote p.author {
	color: whitesmoke;
	font-size: large;
}
/* Gestreifter Hintergrund https://css-tricks.com/stripes-css/ */
.stripe-2 {
	color: white;
	background: repeating-linear-gradient(
		-55deg,
		#222,
		#222 10px,
		#333 10px,
		#333 20px
	);
}'); ?>
<!-- blockquote END -->
	<script src="https://use.fontawesome.com/releases/v5.1.1/js/all.js" data-auto-replace-svg="nest"></script>
	<script src="https://cdn.normanhuth.com/assets/prism/js/prism.min.js"></script>
</body>
</html>
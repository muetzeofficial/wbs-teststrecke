<?php

include_once($_SERVER['DOCUMENT_ROOT'].'/sys/functions.php')

?>

<!DOCTYPE html>
<html lang="de">
<head>
	<meta charset="UTF-8" />
	<title>Bilder ausrichten und responsive gestallten (Norman)</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="/index.php?mode=css">
	<link rel="stylesheet" href="https://cdn.normanhuth.com/assets/prism/css/prism.min.css">
	<style>
		html {
			max-width: 1000px;
		}
		.content {
			border: 2px solid #ff8c00;
			border-radius: 0.25rem;
			padding: 5px;
			overflow: hidden;
		}
		.img-fluid {
			max-width: 100%;
			height: auto;
		}
		.text-primary {
			color: #007bff;
		}
		a.text-primary:hover, a.text-primary:focus {
			color: #0062cc;
		}
		.img-thumbnail {
			padding: 0.25rem;
			background-color: #fff;
			border: 1px solid #dee2e6;
			border-radius: 0.25rem;
			max-width: 100%;
			height: auto;
		}
		.bg-dark {
			background-color: #343a40;
		}
		.element_center {
			margin-right: auto;
			margin-left: auto;
			display: block;
		}
	</style>
</head>
<body>
	<h1>Responsive Bilder</h1>
	<p>Reponsive Bilder passen sich von der Gr&ouml;&szlig;e an der Seiten- &amp; Browserbreite an.<br>
	Diese Bild hat eine Gr&ouml;&szlig;e von 2000x500 Pixel:</p>
	<div class="content">
		<img src="https://placeholder.normanhuth.de/2000x500/ffffff/111111/250/arial" alt="Image 2000px x 500px" title="Image 2000px x 500px" class="img-fluid">
	</div>
	<?php NHparsePrism('css','max-width: 100%;
height: auto;'); ?>
	<hr style="margin-top: 25px;">
	<h1>Thumbnails Bilder</h1>
	<p>Dieses Bild ist mit abgerundeten Ecken, reponsive &amp; bekommt ein Rahmen.</p>
	<div class="content bg-dark">
		<img src="https://placeholder.normanhuth.de/200x200/ffffff/111111/50/arialbd" alt="Image 200px x 200px" title="Image 200px x 200px" class="img-thumbnail">
	</div>
	<?php NHparsePrism('css','padding: 0.25rem;
background-color: #fff;
border: 1px solid #dee2e6;
border-radius: 0.25rem;
max-width: 100%;
height: auto;'); ?>
	<hr style="margin-top: 25px;">
	<h1>Bilder ausrichten</h1>
	<div class="content">
		<img src="https://placeholder.normanhuth.de/200x200/ffffff/111111/50/arialbd" alt="Image 200px x 200px" title="Image 200px x 200px" style="float:left;">
		<img src="https://placeholder.normanhuth.de/200x200/ffffff/111111/50/arialbd" alt="Image 200px x 200px" title="Image 200px x 200px" style="float:right;">
		Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	</div>
	Das Element nach links oder rechts ausrichten:
	<?php NHparsePrism('css','float: left;
float: right;'); ?>
	HTML-Code f&uuml;r dieses Beispiel
	<?php NHparsePrism('html','<img src="https://placeholder.normanhuth.de/200x200/ffffff/111111/50/arialbd" alt="Image 200px x 200px" title="Image 200px x 200px" style="float:left;">
<img src="https://placeholder.normanhuth.de/200x200/d3d3d3/111111/50/arialbd" alt="Image 200px x 200px" title="Image 200px x 200px" style="float:right;">
Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','markup'); ?>
	<hr style="margin-top: 25px;">
	<h1>Bilder zentrieren</h1>
	<div class="content">
		<img src="https://placeholder.normanhuth.de/200x200/ffffff/111111/50/arialbd" alt="Image 200px x 200px" title="Image 200px x 200px" class="element_center">
	</div>
	<?php NHparsePrism('css','margin-right: auto;
margin-left: auto;
display: block;'); ?>
	<script src="https://use.fontawesome.com/releases/v5.1.1/js/all.js" data-auto-replace-svg="nest"></script>
	<script src="https://cdn.normanhuth.com/assets/prism/js/prism.min.js"></script>
</body>
</html>
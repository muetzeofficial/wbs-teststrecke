<?php
//	SETTINGS

	# Ordner
$_folder		= 'content';
	# Datei- & Ordnerfilter
$_filter	= array('.htaccess','index.php','teststrecke_script.php');
	# Erlaubte Dateiendungen
$_ext		= array('html','php','htm','htm','html5','xml','txt');


#########################################################################
##                             ###########################################
##   (c) 2018 Norman Huth      ############################################
##   normanhuth.com            #############################################
##   normanhuth.de             #############################################
##   contact@normanhuth.com    ############################################
##                             ###########################################
#########################################################################

$_folder = substr($_folder,-1)=='/'	 ? $_folder : $_folder.'/';
$_folder = substr($_folder,0,1)=='/' ? substr($_folder,1) : $_folder;

function NHparsePrism($lang,$string,$linenums=1) {
	$lang = $lang=='html' || $lang=='HTML' ? 'markup':$lang;
	$lnum = $linenums==1 ? ' class="line-numbers"':'';
	echo '<pre'.$lnum.'><code class="language-'.strtolower($lang).'">'.htmlspecialchars($string).'</code></pre>';
}

function minifyCss($css) {
	$css = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css);
	preg_match_all('/(\'[^\']*?\'|"[^"]*?")/ims', $css, $hit, PREG_PATTERN_ORDER);
	for ($i=0; $i < count($hit[1]); $i++) {
		$css = str_replace($hit[1][$i], '##########' . $i . '##########', $css);
	}
	$css = preg_replace('/;[\s\r\n\t]*?}[\s\r\n\t]*/ims', "}\r\n", $css);
	$css = preg_replace('/;[\s\r\n\t]*?([\r\n]?[^\s\r\n\t])/ims', ';$1', $css);
	$css = preg_replace('/[\s\r\n\t]*:[\s\r\n\t]*?([^\s\r\n\t])/ims', ':$1', $css);
	$css = preg_replace('/[\s\r\n\t]*,[\s\r\n\t]*?([^\s\r\n\t])/ims', ',$1', $css);
	$css = preg_replace('/[\s\r\n\t]*{[\s\r\n\t]*?([^\s\r\n\t])/ims', '{$1', $css);
	$css = preg_replace('/([\d\.]+)[\s\r\n\t]+(px|em|pt|%)/ims', '$1$2', $css);
	$css = preg_replace('/([^\d\.]0)(px|em|pt|%)/ims', '$1', $css);
	$css = preg_replace('/\p{Zs}+/ims',' ', $css);
	$css = str_replace(array("\r\n", "\r", "\n"), '', $css);
	for ($i=0; $i < count($hit[1]); $i++) {
		$css = str_replace('##########' . $i . '##########', $hit[1][$i], $css);
	}
	return $css;
}

if(isset($_GET['site']) && $_GET['site']!='') {
	echo '<!DOCTYPE html>
<html lang="de">
<!--

	#########################################################################
	##                             ###########################################
	##   (c) 2018 Norman Huth      ############################################
	##   normanhuth.com            #############################################
	##   normanhuth.de             ############################################
	##                             ###########################################
	#########################################################################

-->
<head>
	<meta charset="UTF-8" />
	<title>WBS &Uuml;bungsbeispiele Webprogrammierung</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" type="image/x-icon" href="https://normanhuth.com/res/favicon.ico">
	<link rel="stylesheet" href="https://cdn.normanhuth.com/assets/prism/css/prism.min.css">
	<style>
		body {
			padding-top: 56px;
			display: block;
			overflow: hidden;
		}
		.bg-purple {
			background-color: #493266 !important
		}
		.nav-link {
			cursor: pointer;
		}
	</style>
</head>
<body>
	<nav class="navbar navbar-expand-md fixed-top navbar-dark bg-purple">
		<a href="/" style="font-size: 22px; font-family: \'Monotype Corsiva\',\'Apple Chancery\',\'ITC Zapf Chancery\',\'URW Chancery L\',cursive;" class="navlink text-warning"><span class="fas fa-magic"></span>Beispielseiten &Uuml;besrichts</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    		<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<div class="nav-link" onclick="openextern()">
						<span class="fas fa-external-link-alt"></span>
						Seite im eigenen Fenster &ouml;ffnen
					</div>
				</li>
				<li class="nav-item">
					<div class="nav-link" onclick="openvalidator()">
						<span class="fab fa-html5"></span>
						Seite mit Validator &uuml;berpr&uuml;fen
					</div>
				</li>
			</ul>
		</div>
	</nav>
	<div>
		<iframe onload="load()" src="/'.$_folder.rawurlencode($_GET['site']).'" style="display: block;position: absolute; height: 94%; width: 100%; border: none;" id="myframe"></iframe>
	</div>

	<script src="https://use.fontawesome.com/releases/v5.1.1/js/all.js" data-auto-replace-svg="nest"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script>
	function openextern() {
		var url = document.getElementById("myframe").contentWindow.location.href
    	window.open(url);
	}
	function openvalidator() {
		var url = document.getElementById("myframe").contentWindow.location.href
    	window.open("https://validator.w3.org/check?uri="+url);
	}
	$("iframe").on(\'load\',function() {
		document.title = document.getElementById("myframe").contentDocument.title;
	});
	</script>
</body>
</html>';
	exit();
	
	
}
if(isset($_GET['mode'])) {
	if($_GET['mode']=='css') {
		$css='html {
					height: 100%;
					box-sizing: border-box;
					margin: 0 auto;
					font-family: Verdana;
				}
				*,
				*:before,
				*:after {
					box-sizing: inherit;
				}
				body {
					position: relative;
					margin: 0;
					padding-top: 1rem;
					padding-bottom: 4rem;
					min-height: 100%;
					background-color: rgba(192,192,192,0.8);
					border: none;
					width: 100%
				}
				.card-header-custom {
					border-radius: .25rem .25rem 0 0;
					padding: .75rem 1.25rem;
					background-color: #f5f5f5;
					border: 1px solid #e5e5e5;
					border-bottom: none;
				}
				.card-header-custom + .card-group > .card {
					border-top-left-radius: 0;
					border-top-right-radius: 0;
				}
				.dropdown-menu {
					white-space: nowrap;
					padding-left: 10px;
					padding-right: 10px;
				}
				a.dropdown-item {
					color: #d39e00 !important;
				}
				a.dropdown-item:hover,a.dropdown-item:focus {
					color: #d39e00 !important;
				}
				.bg-purple {
					background-color: #493266 !important;
				}
				.navlink {
					font-size: x-large;
					padding: 0rem 1rem;
					padding-right: 0;
					padding-left: 0;
				}
				a.navlink {
					color: #a79bb1;
				}
				a.navlink:hover {
					color: #d3cdd8;
					text-decoration: none;
				}
				.card-group>.card {
					margin-bottom: 0px !important;
				}
				#btn_ttt {
					display: none;
					position: fixed;
					bottom: 30px;
					right: 30px;
					z-index: 99;
				}';
	}
	header('Content-type: text/css');
	echo minifyCss($css);
	exit();
}

$file_row	='';
$files		= scandir($_folder);
$i=0;
foreach($files as $file) {
	if($file!='.' && $file!='..' && !in_array($file,$_filter)) {
		if(is_dir($_folder.$file)) {
			$i++;
		} else {
			$ext = substr(strrchr(strtolower($file),'.'),1);
			if(in_array($ext,$_ext)) {
				$i++;
			}
		}
	}
}

$new_array	= array();
natcasesort($files);
$break = ceil($i/2);

$i=0;
foreach($files as $file) {
	if($file!='.' && $file!='..' && !in_array($file,$_filter)) {
		if(is_dir($_folder.$file)) {
			#$file_row.='<li class="list-group-item"><a href="index.php?site='.rawurlencode($file).'">'.$file.' '.date('d.m.Y G:i',filemtime($_folder.$file)).'</a></li>';
			$file_row.='<li class="list-group-item"><a href="/index.php?site='.rawurlencode($file).'">'.$file.'</a></li>';
			$i++;
			$new_array[$file] = filemtime($_folder.$file);
		} else {
			#$file_row.='<li class="list-group-item"><a href="index.php?site='.rawurlencode($file).'">File: '.$file.' '.date('d.m.Y G:i',filemtime($_folder.$file)).'</a></li>';
			$ext = substr(strrchr(strtolower($file),'.'),1);
			if(in_array($ext,$_ext)) {
				$file_row.='<li class="list-group-item"><a href="/index.php?site='.rawurlencode($file).'">'.str_ireplace('.'.$ext,'',$file).'</a></li>';
				$i++;
				$new_array[$file] = filemtime($_folder.$file);
			}
		}
		if($i==$break) {
			$file_row.='			</ul>
		</div>
		<div class="card">
			<ul class="list-group list-group-flush">';
		}
	}
}
arsort($new_array);
foreach($new_array as $key => $value) {
	if(is_dir($_folder.$key)) {
		$newest = '<a class="text-dark" href="/index.php?site='.rawurlencode($key).'">'.$key.'</a>';
	} else {
		$ext = substr(strrchr(strtolower($key),'.'),1);
		$newest = '<a class="text-dark" href="/index.php?site='.rawurlencode($key).'">'.str_ireplace('.'.$ext,'',$key).'</a>';
	}
	$newest_time	= date('d.m.Y  G:i:s',$value);
	break;
}

?>
<!DOCTYPE html>
<html lang="de">
<!--

	#########################################################################
	##                             ###########################################
	##   (c) 2018 Norman Huth      ############################################
	##   normanhuth.com            #############################################
	##   normanhuth.de             ############################################
	##                             ###########################################
	#########################################################################

-->
<head>
	<meta charset="UTF-8" />
	<title>WBS &Uuml;bungsbeispiele Webprogrammierung</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" type="image/x-icon" href="https://normanhuth.com/res/favicon.ico">
	<link rel="stylesheet" href="https://cdn.normanhuth.com/assets/prism/css/prism.min.css">
	<link rel="stylesheet" href="/index.php?mode=css">
	<link rel="stylesheet" href="https://syntaxhigh.normanhuth.com/res/prism/prism.min.css">
	<style>
		body {
			padding-top: 5rem;
		}
		html {
			max-width: 1000px;
		}
	</style>
</head>
<body>
	<button onclick="topFunction()" id="btn_ttt" title="Go to top" class="btn btn-info"><span class="fas fa-arrow-alt-circle-up"></span></button>
<nav class="navbar fixed-top navbar-dark bg-purple" id="topnav">
		<div class="dropdown">
			<button type="button" class="btn btn-warning dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				Webmaster Tools
			</button>
			<div class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton">
				<a class="dropdown-item" href="http://tomheller.de/html-farben.html" target="_blank">Farbcodes HEX &amp; RGB <sup><span class="fas fa-external-link-alt"></span></sup></a>
				<a class="dropdown-item" href="https://normanhuth.de/diff" target="_blank">2 Dateien oder Texte vergleichen <sup><span class="fas fa-external-link-alt"></span></sup></a>
				<a class="dropdown-item" href="https://validator.w3.org" target="_blank">HTML Validator <sup><span class="fas fa-external-link-alt"></span></sup></a>
				<a class="dropdown-item" href="https://normanhuth.de/urlcoder" target="_blank">URL nach RFC 3986 kodieren &amp; dekodieren <sup><span class="fas fa-external-link-alt"></span></sup></a>
				<a class="dropdown-item" href="https://wbs.normanhuth.com/codesnippets" target="_blank">Hilfe &amp; Codeschnipsel <sup><span class="fas fa-external-link-alt"></span></sup></a>
			</div>
		</div>
		<ul class="navbar-nav ml-auto">
			<li class="nav-item">
				<a href="https://validator.w3.org/check?uri=<?php echo rawurlencode('https://teststrecke.normanhuth.de'); ?>" target="_blank" class="navlink" title="HTML5 Valide">
					<span class="fab fa-html5"></span>
					<span class="fas fa-check"></span>
				</a>
			</li>
		</ul>
	</nav>
<?php	
	if(isset($_GET['err']) && $_GET['err']=='404') {
		echo '
<div class="alert alert-danger" role="alert">
	<h4 class="alert-heading">404 Not found <span class="far fa-sad-cry"></span></h4>
	<hr>
	<p class="mb-0">Unter dieser URL wurde Nichts gefunden auf diesem Server.</p>
	<p>Schau doch einfach mal im Inhaltsverzeichnis, ob Du f&uuml;ndig wirst...</p>
</div>';
	}
	if(isset($_GET['err']) && $_GET['err']=='403') {
		echo '
<div class="alert alert-danger" role="alert">
	<h4 class="alert-heading">403 Forbidden <span class="fas fa-shield-alt"></span></h4>
	<hr>
	<p class="mb-0">Zugriff verweigert.</p>
	<p>Der (direkte) Aufruf zu dem gew&auml;hlen Bereich ist nicht gestattet.</p>
</div>';
	}
	
	?>
	<div class="card-header-custom text-center">
		<div class="font-weight-bold" style="font-size: x-large">
			Beispielseiten &amp; mehr
		</div>
	</div>
	<div class="card-group">
		<div class="card">
			<ul class="list-group list-group-flush">
<?php echo $file_row; ?>
			</ul>
		</div>
	</div>
	<div class="text-center small text-muted">
		Zuletzt aktualisiert (<?php echo $newest_time; ?>): <?php echo $newest ?>		
	</div>
	<div class="jumbotron jumbotron-flui shadow mt-4 pt-4">
		<div class="h3 text-center"><span class="far fa-star"></span> Eigene Beispiele &amp; Seiten hochladen <span class="far fa-thumbs-up"></span></div>
		<ul class="list-group">
			<li class="list-group-item">
				<div class="container">
					<div class="row">
						<div class="col-">
							Schritt <span class="badge badge-secondary">1</span>:
						</div>
						<div class="col-lg">
							Eine einzelne Datei schreiben oder ein ganzen Projekt in einem Ordner
						</div>
					</div>
				</div>
			</li>
			<li class="list-group-item">
				<div class="container">
					<div class="row">
						<div class="col-">
							Schritt <span class="badge badge-secondary">2</span>:
						</div>
						<div class="col-lg">
							Datei oder Ordner so bennen, wie es im Inhaltsverzeichnis stehen soll
						</div>
					</div>
				</div>
			</li>
			<li class="list-group-item">
				<div class="container">
					<div class="row">
						<div class="col-">
							Schritt <span class="badge badge-secondary">3</span>:
						</div>
						<div class="col-lg">
							FTP-Daten geben lassen (von Norman)
						</div>
					</div>
				</div>
			</li>
			<li class="list-group-item">
				<div class="container">
					<div class="row">
						<div class="col-">
							Schritt <span class="badge badge-secondary">4</span>:
						</div>
						<div class="col-lg">
							Eigene Datei oder Ordner hochladen
						</div>
					</div>
				</div>
			</li>
			<li class="list-group-item">
				<div class="container">
					<div class="row">
						<div class="col-">
							Hinweis:
						</div>
						<div class="col-lg">
							Es gibt keine Einschr&auml;nkungen bei den hoch geladenen Beispielen.
							<p class="text-muted small">Aktuell ist PHP Version 7.0 als <span style="white-space: nowrap;">Apache-Modul</span> aktiviert &amp; auf Wunsch kann auch PHP 7.1 oder 7.2 als <span style="white-space: nowrap;">CGI-Modul</span> aktiviert werden</p>
							<div class="alert alert-success" role="alert">
								Auf Wunsch gibt es auch eine <strong>MySQL</strong>-Datenbank
							</div>
						</div>
					</div>
				</div>				
			</li>
			<li class="list-group-item">
				<div class="container">
					<div class="row">
						<div class="col-">
							Optional:
						</div>
						<div class="col-lg">
							<a href="#" class="text-primary" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
								Syntax Highlighter in einer <strong>PHP</strong>-Datei mit einbinden &amp; nutzen <span class="badge badge-danger"><span class="fas fa-star"></span> New <span class="fas fa-star"></span></span>
							</a>
							<div class="collapse" id="collapseExample">
								<div class="border border-primary p-1 rounded-bottom border-top-0">
									Gleich am Anfang der PHP-Datei folgenen Code einf&uuml;gen:
    								<?php NHparsePrism('php','<?php

include_once($_SERVER[\'DOCUMENT_ROOT\'].\'/sys/functions.php\')

?>'); ?>
									<hr>
									Im <code><?php echo htmlspecialchars('<head>...</head>'); ?></code> Bereich folgenden Code einf&uuml;gen:
									<?php NHparsePrism('html','<link rel="stylesheet" href="https://syntaxhigh.normanhuth.com/res/prism/prism.min.css">',0); ?>
									<strong>Vor</strong> dem schlie&szlig;enden <code><?php echo htmlspecialchars('</body>'); ?></code>-Tag folgenden Code einf&uuml;gen:
									<?php NHparsePrism('html','<script src="https://cdn.normanhuth.com/assets/prism/js/prism.min.js"></script>',0); ?>
									<hr>
									Dann kann man an jeder Stelle an dem man den Syntax Highlighter benuzen m&ouml;chte dies mit folgender Funktion:
									<?php NHparsePrism('php','<?php NHparsePrism(\'html\',\'<code onclick="alert(\\\'Hello! I am an alert box!!\\\');">Mein Code</code>\'); ?>',0); ?>
									<strong>Wichtig</strong>: Jedes Apostroph <code>'</code> im Code muss escpaed werden, so: <code>\'</code>
									<p>Sprache (in diesem Beispiel HTML) kann nat&uuml;rlich mit PHP, CSS usw. ersetzt werden</p>
									<hr>
									In der Standardeinstellung wird eine Zeilennummer mit angebenen. Ohne Zeilennummer lautet die Funktion so:
									<?php NHparsePrism('php','<?php NHparsePrism(\'html\',\'<code></code>\',0); ?>',0); ?>
								</div>
							</div>
						</div>
					</div>
				</div>				
			</li>
		</ul>
	</div>
	<script src="https://cdn.normanhuth.com/assets/prism/js/prism.min.js"></script>
	<script src="https://use.fontawesome.com/releases/v5.1.1/js/all.js" data-auto-replace-svg="nest"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script>
	<?php
	
	if(isset($_GET['err']) && ($_GET['err']=='404' || $_GET['err']=='403')) {
		echo '	if(top != self) {
				top.location = self.location;
			}';
	}
		
	?>
	if(top != self) {
		var element = document.getElementById("topnav");
		element.classList.add("d-none");
	}
	window.onscroll = function() {scrollFunction()};
	function scrollFunction() {
		if (document.body.scrollTop > 25 || document.documentElement.scrollTop > 25) {
			document.getElementById("btn_ttt").style.display = "block";
		} else {
			document.getElementById("btn_ttt").style.display = "none";
		}
	}
	function topFunction() {
		document.body.scrollTop = 0;
		document.documentElement.scrollTop = 0;
	}
	</script>
</body>
</html>
<?php

$sites 	= array('Anwendungsentwicklung','Systemintegration','Infos');
$site	= isset($_GET['site']) && $_GET['site']!='' && in_array($_GET['site'],$sites) ? $_GET['site'] : 'Startseite';

function NHgettemplate($template) {
	
    return str_replace("\"","\\\"",implode("",file('content/'.$template.".html")));
}

$site_title = $site=='Startseite' ? 'IT-Fachinformatiker' : htmlspecialchars($site);

eval ("\$body= \"".NHgettemplate($site)."\";");

?>
<!doctype html>
<html lang="de">
<head>
<meta charset="utf-8">
<title><?php echo $site_title; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css">
	<link href="favicon.png" rel="icon" type="image/png" sizes="64x64">
	<link href="favicon.png" rel="shortcut icon" type="image/png"/>
	<link href="favicon.png" rel="apple-touch-icon" sizes="64x64">
	<style>
		.example_gradient {
			background: linear-gradient(#333,#666);
			margin-right: 12px;
		}
		.example_gradient:hover {
			background: linear-gradient(#000,#333);
			margin-right: 12px;
		}
		.activem {
			background: linear-gradient(#111,#444);
		}
		body {
			margin-top: 6rem;
			margin-bottom: 5rem;
			background-color: aliceblue;
		}
	</style>
</head>
<body>
	<nav class="navbar fixed-top navbar-info bg-info">
		<span class="navbar-brand font-weight-bold" style="font-size: 26px;">
			IT-Fachinformatiker &raquo; <?php echo $site; ?>
		</span>
		<ul class="nav nav-pills p-2">
<?php
	$active = $site=='Startseite' ? ' activem' : '';
	echo '<li class="nav-item"><a class="nav-link example_gradient text-light'.$active.'" href="Startseite">Startseite</a></li>';
			
	foreach($sites as $menu) {
		$active = $site==$menu ? ' activem' : '';
		echo '<li class="nav-item"><a class="nav-link example_gradient text-light'.$active.'" href="'.$menu.'">'.$menu.'</a></li>';
	}
?>
		</ul>
	</nav>
	<div class="container">
<?php 
		$body = preg_replace('/\[h\](.+)\[\/h\]/Usi', ' <h1>\\1</h1>', $body);
		$body = preg_replace_callback('/\[nl2br\](.*)\[\/nl2br\]/Us',
					function($matches) {
						return nl2br(trim($matches[1]));
					},$body);
		
		echo $body; 
		
?>
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<!-- script src="https://use.fontawesome.com/releases/v5.1.1/js/all.js" data-auto-replace-svg="nest"></script -->
</body>
</html>
<?php

include_once($_SERVER['DOCUMENT_ROOT'].'/sys/functions.php')

?>

<!DOCTYPE html>
<html lang="de">
<head>
	<meta charset="UTF-8" />
	<title>Icons mit einer Webfont</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" type="image/x-icon" href="https://normanhuth.com/res/favicon.ico">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdn.normanhuth.com/assets/prism/css/prism.min.css">
	<style>
		html {
			height: 100%;
			box-sizing: border-box;
			max-width: 1000px;
			margin: 0 auto;
			font-family: Verdana;
		}
		*,
		*:before,
		*:after {
			box-sizing: inherit;
		}
		body {
			position: relative;
			margin: 0;
			padding-top: 1rem;
			padding-bottom: 4rem;
			min-height: 100%;
			background-color: rgba(192,192,192,0.8);
			border: none;
			width: 100%
		}
	</style>
</head>
<body>
	<div class="container-fluid">
		<h1>Icons
			<span class="fab fa-facebook-square h5"></span>
			<span class="fab fa-twitter h5"></span>
			<span class="fab fa-amazon h5"></span>
			<span class="fab fa-twitter-square h5"></span>
			<span class="far fa-address-book h5"></span>
			<span class="fas fa-address-book h5"></span>
			<span class="fas fa-music h5"></span> mit einer Webfont <span class="far fa-hand-point-right"></span></h1>
		<h2>In diesem Beispiel mit der Webfont: Font Awesome</h2>
		<hr>
		<h3>Schritt 1: Die Schriftart implementieren</h3>
		<p class="small">Die aktuelle Version ist immer auf <a href="https://fontawesome.com/how-to-use/on-the-web/setup/getting-started?using=web-fonts-with-css" target="_blank" class="text-primary">fontawesome.com</a> zu finden</p>
		<h4>M&ouml;glichkeit 1: Per CSS</h4>
		<?php NHparsePrism('html','<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">',0); ?>
		<p>Diesen Code einfach im <code><?php echo htmlspecialchars('<head>...</head>'); ?></code> Bereich einf&uuml;gen</p>
		<h4 class="mt-2">M&ouml;glichkeit 2: Per Javascript</h4>
		<?php NHparsePrism('html','<script defer src="https://use.fontawesome.com/releases/v5.2.0/js/all.js" integrity="sha384-4oV5EgaV02iISL2ban6c/RmotsABqE4yZxZLcYMAdG7FAPsyHYAPpywE9PJo+Khy" crossorigin="anonymous"></script>',0); ?>
		<p>Diesen Code einfach am Ende vor dem <code><?php echo htmlspecialchars('</body>'); ?></code> einf&uuml;gen</p>
		<hr>
		<h3>Schritt 2: Icon im einf&uuml;gen</h3>
		<p>Dazu einfach in die <a href="https://fontawesome.com/icons?d=gallery&amp;m=free" target="_blank" class="text-primary font-weight-bold">Font Awesome Galerie</a> gehen und das gew&uuml;nschte Icon raus suchen &amp; &ouml;ffnen.</p>
		<p>In diesem Beispiel habe ich mir das smile-wink <span class="far fa-smile-wink"></span> raus gesucht</p>
		<p>Wenn man das Icon ge&ouml;ffnet hat steht darunter:</p>
		<?php  NHparsePrism('html','<i class="far fa-smile-wink"></i>',0); ?>
		<p>Diesen Code muss dann nur noch an der Stelle in seinem Quelltext einf&uuml;gen wo man das Icon hin haben m&ouml;chte</p>
	</div>
	<script src="https://use.fontawesome.com/releases/v5.1.1/js/all.js" data-auto-replace-svg="nest"></script>
	<script src="https://cdn.normanhuth.com/assets/prism/js/prism.min.js"></script>
</body>
</html>
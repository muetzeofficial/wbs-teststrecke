<!doctype html>
<html lang="de">
<head>
	<meta charset="utf-8">
	<title>HTML_GH</title>
	<style>
		body {
			font-family: "Gill Sans", "Gill Sans MT", "Myriad Pro", "DejaVu Sans Condensed", Helvetica, Arial, "sans-serif";
			padding-top: 20px;
			max-width: 700px;
			margin: 0 auto;
		}
		.mt-spacer {
			margin-top: 20px;
		}
		select {
			width: 150px;
			overflow: hidden;
		}
	</style>
</head>
<body>
	<form action="HTML_GH.php" method="post">
		<div>
			<h1>W&auml;hlen Sie das gew&uuml;nschte Konzert aus:</h1>
		</div>
		<div>
			<select name="konzert" size="3">
				<option value="konzert0">Singing Birds</option>
				<option value="konzert1">Rolling Voices</option>
				<option value="konzert2">Black Monday</option>
			</select>
		</div>
		<div class="mt-spacer">
			Preiskategorie
		</div>
		<div>
			<input type="radio" name="preis" value="a"> A - 35 Euro
		</div>
		<div>
			<input type="radio" name="preis" value="b"> B - 45 Euro
		</div>
		<div>
			<input type="radio" name="preis" value="c"> C - 55 Euro
		</div>
		<div class="mt-spacer">
			Anzahl der Karten:
			<input type="number" name="anzahl">
		</div>
		<div>
			<!-- input type="submit" value="Buchen" -->
			<button>Buchen</button>
		</div>
	</form>
</body>
</html>
<?php

$cms = array(
	'Concrete5 (V8)'		 									=> 'https://concrete5-v8.teststrecke.normanhuth.de',
	'Drupal (V7)' 												=> 'https://drupal-v7.teststrecke.normanhuth.de',
	'Drupal (V8)' 												=> 'https://drupal-v8.teststrecke.normanhuth.de',
	'Joomla (V3)' 												=> 'https://joomla-v3.teststrecke.normanhuth.de',
	'SilverStripe (V4)' 										=> 'https://silverstripe-v4.teststrecke.normanhuth.de',
	'WordPress (V4)'	 										=> 'https://wordpress-v4.teststrecke.normanhuth.de',
	'Typo3 (V7)' 												=> 'https://typo3-v7.teststrecke.normanhuth.de/typo3/',
	'Typo3 (V8)' 												=> 'https://typo3-v8.teststrecke.normanhuth.de/typo3/',
	'Xoops (V2)' 												=> 'http://xoops-v2.teststrecke.normanhuth.de',
	'Contao (V3)' 												=> 'https://contao-v3.teststrecke.normanhuth.de',
	'WoltLab Suite (V3) [badge=red]kostenpflichtig[/badge]'		=> 'https://wwb-v3.teststrecke.normanhuth.de',
	'Typesetter (V5) [badge]Ohne Datenbank[/badge]'				=> 'https://typesetter-v5.teststrecke.normanhuth.de',
);

$blog = array(
	'WordPress (V4)' 											=> 'https://wordpress-v4.teststrecke.normanhuth.de',
	'Drupal (V7)' 												=> 'https://drupal-v7.teststrecke.normanhuth.de',
	'Drupal (V8)' 												=> 'https://drupal-v8.teststrecke.normanhuth.de',
	'Joomla (V3)' 												=> 'https://joomla-v3.teststrecke.normanhuth.de',
	'SilverStripe (V4)'									 		=> 'https://silverstripe-v4.teststrecke.normanhuth.de',
);

$forum = array(
	'MyBB'			 											=> 'http://mybb.teststrecke.normanhuth.de',
	'phpBB (V3)'												=> 'https://phpbb-3.teststrecke.normanhuth.de',
	'WoltLab Suite (V3) [badge=red]kostenpflichtig[/badge]'		=> 'https://wwb-v3.teststrecke.normanhuth.de',
	'Simple Machines Forum (V2)'								=> 'https://smf-v2.teststrecke.normanhuth.de',
);

$shop = array(
	'Magento 2'			 										=> 'https://www.splendid-internet.de/magento-2-demo/',
	'PrestaShop'			 									=> 'https://prestashop.teststrecke.normanhuth.de',
	'Shopware (V5)'								 				=> 'https://shopware-v5.teststrecke.normanhuth.de',
);

$misc = array(
	'Matins Bug Tracker (V2)'									=> 'https://mantis-v2.teststrecke.normanhuth.de',
	'Media Wiki'												=> 'https://media-wiki.teststrecke.normanhuth.de',
	'Blue Spice (V2)'											=> 'https://bluespice-v2.teststrecke.normanhuth.de',
	'Flyspray'													=> 'https://flyspray.teststrecke.normanhuth.de',
	'Tiki Wiki'													=> 'https://tiki.teststrecke.normanhuth.de',
	'Help Desk Software HESK (V2)'								=> 'https://hesk-v2.teststrecke.normanhuth.de',
	'PHP Sudoku'												=> 'http://phpsudoku.teststrecke.normanhuth.de',
);

$cloud = array(
	'OwnCloud (V10)'											=> 'https://owncloud-v10.teststrecke.normanhuth.de',
	'nextcloud (V13)'											=> 'https://nextcloud-v13.teststrecke.normanhuth.de',
	'Pydio (V8)'												=> 'https://pydio-v8.teststrecke.normanhuth.de',
);


#########################################################################
##                             ###########################################
##   (c) 2018 Norman Huth      ############################################
##   normanhuth.com            #############################################
##   normanhuth.de             #############################################
##   contact@normanhuth.com    ############################################
##                             ###########################################
#########################################################################

function NHurlname($string) {
	$string = htmlspecialchars(trim($string));
	
	$string = preg_replace('/\[badge\](.+)\[\/badge\]/Usi', '<span class="badge badge-info">\\1</span>', $string);
	$string = preg_replace('/\[badge=red\](.+)\[\/badge\]/Usi', '<span class="badge badge-danger">\\1</span>', $string);
	
	#$string = str_replace('http:','https:',$string);
	
	return $string;
}
?>

<!DOCTYPE html>
<html lang="de">
<head>
	<meta charset="UTF-8" />
	<title>CMS-, Blog-, Shop &amp; andere Systeme</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="/index.php?mode=css">
	<style>
		body {
			background-color: #c0c0c0;
			background: linear-gradient(#aaaaaa,#dddddd);
			background-attachment: fixed;
			min-height: 100%;
		}
		html {
			max-width: 500px;
			height: 100%;
		}
		.pointer {
			cursor: pointer;
		}
	</style>
</head>
<body>
	<div class="accordion" id="accordionExample">
		<div class="card rounded border border-secondary shadow">
			<div class="card-header pointer" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
				 <strong>C</strong>ontent-<strong>M</strong>anagement-<strong>S</strong>ysteme
			</div>
			<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
				<div class="card-body">
					<div class="list-group text-center">
						<?php

						ksort($cms);

						foreach ($cms as $key => $value) {
							echo '<a href="'.rawurldecode(trim($value)).'" class="list-group-item list-group-item-action" target="_blank">'.NHurlname($key).'</a>';
						}

						?>
					</div>
				</div>
			</div>
			<div class="card-header pointer" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
				Blog Systeme
			</div>
			<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
				<div class="card-body">
					<div class="list-group text-center">
						<?php

						ksort($blog);

						foreach ($blog as $key => $value) {
							echo '<a href="'.rawurldecode(trim($value)).'" class="list-group-item list-group-item-action" target="_blank">'.NHurlname($key).'</a>';
						}

						?>
					</div>
				</div>
			</div>
			<div class="card-header pointer" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
				Foren Systeme
			</div>
			<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
				<div class="card-body">
					<div class="list-group text-center">
						<?php

						ksort($forum);

						foreach ($forum as $key => $value) {
							echo '<a href="'.rawurldecode(trim($value)).'" class="list-group-item list-group-item-action" target="_blank">'.NHurlname($key).'</a>';
						}

						?>
					</div>
				</div>
			</div>
			<div class="card-header pointer" id="headingFour" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
				Shop Systeme
			</div>
			<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
				<div class="card-body">
					<div class="list-group text-center">
						<?php

						ksort($shop);

						foreach ($shop as $key => $value) {
							echo '<a href="'.rawurldecode(trim($value)).'" class="list-group-item list-group-item-action" target="_blank">'.NHurlname($key).'</a>';
						}

						?>
					</div>
				</div>
			</div>
			<div class="card-header pointer" id="headingFive" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
				Cloud Systeme
			</div>
			<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
				<div class="card-body">
					<div class="list-group text-center">
						<?php

						ksort($cloud);

						foreach ($cloud as $key => $value) {
							echo '<a href="'.rawurldecode(trim($value)).'" class="list-group-item list-group-item-action" target="_blank">'.NHurlname($key).'</a>';
						}

						?>
					</div>
				</div>
			</div>
			<div class="card-header pointer" id="headingSix" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
				Sonstige Systeme
			</div>
			<div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
				<div class="card-body">
					<div class="list-group text-center">
						<?php

						ksort($misc);

						foreach ($misc as $key => $value) {
							echo '<a href="'.rawurldecode(trim($value)).'" class="list-group-item list-group-item-action" target="_blank">'.NHurlname($key).'</a>';
						}

						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
</body>
</html>
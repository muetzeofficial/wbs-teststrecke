<?php
// Inhalte dieser Datei: Variablen, Assoziatives Array, die Funktion mt_rand (rand)
// Norman Huth 2018

$npc_img		= '<i class="fas fa-question fa-3x"></i>';				// Fragezeichen als Standard
$pt_user		= 0;													// Punkte vom Spieler
$pt_npc			= 0;													// Punkte vom NPC
$message		= 'Bitte treffe eine Wahl';								// Nachricht definieren
$loose			= 'Verloren';											// Verlierer Nachricht
$win			= 'Gewonnen';											// Geweinner Nachricht
$draw			= 'Unentschieden';										// Unentschieden


/***** Einfach nicht beachten das hier **************************\
                                                               \*/
$loose = '<div class="alert alert-danger">'.$loose.'</div>';    #
$win = '<div class="alert alert-success">'.$win.'</div>';       #
$draw = '<div class="alert alert-warning">'.$draw.'</div>';     #
$message = '<div class="alert alert-info">'.$message.'</div>';  #
                                                               /*\
\****************************************************************/

if(isset($_POST['auswahl'])) {
	
	$npc 		= mt_rand(0,2);											// Zufallszahl zwischen 0 & 2
	$pt_user	= $_POST['pt_user'];									// Punkte vom Spieler übernehmen
	$pt_npc		= $_POST['pt_npc'];										// Punkte vom NPC übernehmen
	/*
	0	Schere
	1	Stein
	2	Papier
	*/
	$_img = array(														// Bilder in ein Array packen für Bilder
		'0' => 'schere',
		'1' => 'stein',
		'2' => 'papier'
	);
	if(isset($_POST['auswahl']) && $_POST['auswahl']!='') {				// Nur wenn Auswahl getroffen wurde das Bild per Array Value in $message setzen
		
		$npc_img = '<span class="btn btn-secondary p-0"><img src="'.$_img[$npc].'.png" alt="" class="img-thumbnail npc"></span>';
		switch ($_POST['auswahl']) {
			case 'schere':														// Spieler wählt Schere
				if($npc==0) {
					$message = $draw;											// Unentschieden
				}elseif($npc==1) {
					$message = $loose;											// Verloren
					$pt_npc++;													// NPC Punkte um 1 erhöhen
				}elseif($npc==2) {
					$message = $win;											// Gewonnen
					$pt_user = $pt_user+1;										// Spieler Punkte erhöhen mit normaler Addition
				}
				break;
			case 'stein':
				if($npc==0) {
					$message = $win;
					$pt_user++;
				}elseif($npc==1) {
					$message = $draw;
				}elseif($npc==2) {
					$message = $loose;
					$pt_npc++;
				}
				break;
			case 'papier':
				if($npc==0) {
					$message = $loose;
					$pt_npc++;
				}elseif($npc==1) {
					$message = $win;
					$pt_user++;
				}elseif($npc==2) {
					$message = $draw;
				}
		}
	}
}

?>
<!DOCTYPE html>
<html lang="de">
<head>
	<meta charset="UTF-8" />
	<title>Minispiel - Schere, Stein, Papier</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css">
	<style>
		html {
			max-width: 800px;
			margin: 0 auto;
		}
		.check {
			opacity:0.5;
			color:#000;
		}
		.alert {
			max-width: 300px;
			margin: 0 auto;
		}
		.npc {
			transform: scaleX(-1);
		}
	</style>
	</head>
<body>
	<form action="index.php" method="post">
		<input type="hidden" name="pt_user" value="<?php echo $pt_user; ?>">
		<input type="hidden" name="pt_npc" value="<?php echo $pt_npc; ?>">
		<div class="container mt-4">
			<div class="text-center mb-1">
				<span class="badge badge-success" style="font-size: large;"><?php echo $pt_user; ?></span> : <span class="badge badge-danger" style="font-size: large;"><?php echo $pt_npc; ?></span>
			</div>
			<div class="row shadow">
				<div class="col-sm border py-5 text-center rounded-left">
					<div class="form-group mb-0">
						<button class="btn btn-secondary p-0" name="auswahl" value="schere">
							<img src="schere.png" alt="Schere" class="img-thumbnail img-check">
						</button>
						<button class="btn btn-secondary p-0" name="auswahl" value="stein">
							<img src="stein.png" alt="Stein" class="img-thumbnail img-check">
						</button>
						<button class="btn btn-secondary p-0" name="auswahl" value="papier">
							<img src="papier.png" alt="Papier" class="img-thumbnail img-check">
						</button>
					</div>
				</div>
				<div class="col-sm border border-left-0 py-5 text-center rounded-right">
					<?php echo $npc_img; ?>
				</div>
			</div>
			<div class="text-center mt-4">
				<?php echo $message; ?>
			</div>
			<div class="text-center my-5 text-center text-muted small">
				Regeln und Bilderquelle <a href="https://de.wikipedia.org/wiki/Schere,_Stein,_Papier" target="_blank" class="text-info">de.wikipedia.org</a>
			</div>
		</div>
	</form>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="https://use.fontawesome.com/releases/v5.1.1/js/all.js" data-auto-replace-svg="nest"></script>

</body>
</html>
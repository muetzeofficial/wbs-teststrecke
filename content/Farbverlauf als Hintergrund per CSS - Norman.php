<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/sys/functions.php');

$opt1 = ' selected';
$opt2=$opt3=$opt4=$opt5='';

if(isset($_POST['change_example'])) {
	$opt1 = isset($_POST['change_example']) && $_POST['change_example']==1 ? ' selected':'';
	$opt2 = isset($_POST['change_example']) && $_POST['change_example']==2 ? ' selected':'';
	$opt3 = isset($_POST['change_example']) && $_POST['change_example']==3 ? ' selected':'';
	$opt4 = isset($_POST['change_example']) && $_POST['change_example']==4 ? ' selected':'';
	$opt5 = isset($_POST['change_example']) && $_POST['change_example']==5 ? ' selected':'';
}

$opt = isset($_POST['change_example']) && $_POST['change_example']>0 ? $_POST['change_example'] : 1;

if($opt==2) {
	$body_bg	= 'background: linear-gradient(to bottom right,red,yellow);';
	$div_bg		= 'background: linear-gradient(to right,rgba(0,0,128),rgba(128,0,128));';
} elseif($opt==3) {
	$body_bg	= 'background: linear-gradient(to right, red,orange,yellow,green,blue,indigo,violet);';
	$div_bg		= 'background: linear-gradient(-90deg, skyblue,thistle);';
} elseif($opt==4) {
	$body_bg	= 'background: repeating-linear-gradient(red, yellow 10%, green 20%);';
	$div_bg		= 'background: radial-gradient(red, yellow, green);';
} elseif($opt==5) {
	$body_bg	= 'background: radial-gradient(circle, red, yellow, green);';
	$div_bg		= 'background: repeating-radial-gradient(red, yellow 10%, green 15%);';
} else {
	$body_bg	= 'background: linear-gradient(#000000,silver);';
	$div_bg		= 'background: linear-gradient(to right,violet,springgreen);';
}

?><!DOCTYPE html>
<html lang="de">
<head>
<meta charset="utf-8">
<title>Hintegrund mit Farbverlauf</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdn.normanhuth.com/assets/prism/css/prism.min.css">
	<style>
		html {
			height: 100%;
		}
		body {
			height: 100%;
			<?php echo $body_bg; ?>
			padding-top: 1rem;
			background-attachment: fixed;
		}
		.example_gradient {
			<?php echo $div_bg; ?>
		}
		.card {
			max-width: 1000px;
			margin: 0 auto;
		}
	</style>
</head>
<body>
	<div class="card">
		<div class="card-header">
			Beispiel DIV mit der Klasse &quot;example_gradient&quot;:
		</div>
		<div class="card-body example_gradient">
			&nbsp;
		</div>
		<div class="card-body">
			<form action="<?php echo rawurlencode(NHaction()); ?>" method="post" class="form-inline">
				<div class="form-group mb-2">
					<label for="change_example">Anderes Beispiel anzeigen:&nbsp;</label>
					<select class="form-control" name="change_example" id="change_example" onchange="this.form.submit()">
						<option value="1"<?php echo $opt1; ?>>Beispiel 1</option>
						<option value="2"<?php echo $opt2; ?>>Beispiel 2</option>
						<option value="3"<?php echo $opt3; ?>>Beispiel 3</option>
						<option value="4"<?php echo $opt4; ?>>Beispiel 4</option>
						<option value="5"<?php echo $opt5; ?>>Beispiel 5</option>
					</select>
				</div>
			</form>
		</div>
		<div class="card-footer">
<?php NHparsePrism('css','html {
	height: 100%;
}
body {
	height: 100%;
	'.$body_bg.'
	background-attachment: fixed;
}
.example_gradient {
	'.$div_bg.'
}'); ?>
		</div>
	</div>
	<script src="https://cdn.normanhuth.com/assets/prism/js/prism.min.js"></script>
</body>
</html>
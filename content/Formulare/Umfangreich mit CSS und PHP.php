<?php

$mycheckbox	= isset($_POST['mycheckbox']) && $_POST['mycheckbox']==1		? ' checked':'';
$myradio1	= isset($_POST['myradio']) && $_POST['myradio']==1				? ' checked':'';
$myradio2	= isset($_POST['myradio']) && $_POST['myradio']==2				? ' checked':'';
$myselect1	= isset($_POST['myselect']) && $_POST['myselect']==1			? ' selected':'';
$myselect2	= isset($_POST['myselect']) && $_POST['myselect']==2			? ' selected':'';
$myselect3	= isset($_POST['myselect']) && $_POST['myselect']==3			? ' selected':'';
$mytext		= isset($_POST['mytext']) && $_POST['mytext']!=''				? htmlspecialchars(trim($_POST['mytext'])) : '';
$mytextarea	= isset($_POST['mytextarea']) && $_POST['mytextarea']!=''		? htmlspecialchars(trim($_POST['mytextarea'])) : '';
$myemail	= isset($_POST['myemail']) && $_POST['myemail']!=''				? htmlspecialchars(trim(strtolower($_POST['myemail']))) : '';
if(!isset($_POST['myradio'])) {
	$myradio1 = ' checked';
}

/*
PHP Funktionen:
isset 				= http://php.net/manual/de/function.isset.php 						Um zu prüfen ob Feld gesendet wurde
$_POST[]			= http://php.net/manual/de/reserved.variables.post.php				Verarbeitung des name-Attributes
htmlspecialchars	= http://php.net/manual/de/function.htmlspecialchars.php			Umwandeln von HTML Zeichen, damit kein " im value steht
trim				= http://php.net/manual/de/function.trim.php						Leerzeichen am Amfang & Ende entfernen
strtolower			= http://php.net/manual/de/function.strtolower.php					E-Mail Adresse klein schreiben


*/
$is_invalid = isset($_POST['myemail']) && $_POST['myemail']!='' && filter_var(trim(strtolower($_POST['myemail'])), FILTER_VALIDATE_EMAIL) ? '':' is-invalid';

?>
<!doctype html>
<html lang="de">
<head>
<meta charset="utf-8">
<title>Formular - Umfangreich</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
	<style>
		body {
			max-width: 500px;
			margin: 0 auto;
			padding-top: 2rem;
		}
	</style>
</head>
<body>
	<div class="alert alert-info text-center" role="alert">
		Das Formular merkt sich die gesendeten Daten. Die PHP Datei kann sich ja jeder vom Testserver runter laden....
	</div>
	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" class="bg-light p-2 border rounded">
		<div class="form-group row">
			<div class="col-sm-3">
				<label for="mytext">INPUT Text</label>
			</div>
			<div class="col-sm-9">
				<input type="text" name="mytext" id="mytext" value="<?php echo $mytext; ?>" class="form-control">
			</div>
		</div>
		<div class="form-group row">
			<div class="col-sm-3">
				<label for="myemail">INPUT Email</label>
			</div>
			<div class="col-sm-9">
				<input type="email" name="myemail" id="myemail" value="<?php echo $myemail; ?>" class="form-control<?php echo $is_invalid; ?>" required>
				<small id="emailhelp" class="form-text">
					<div class="text-muted">Dieses Feld ist ein <strong>Pflichtfeld</strong></div>
					<div class="text-success">Gültig mail@host.tld</div>
					<div class="text-danger">Ungültig: mail@host</div>
				</small>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-sm-3">Checkbox</div>
			<div class="col-sm-9">
				<div class="custom-control custom-checkbox">
					<input type="checkbox" value="1" class="custom-control-input" id="mycheckbox" name="mycheckbox"<?php echo $mycheckbox; ?>>
					<label class="custom-control-label" for="mycheckbox">INPUT checkbox</label>
				</div>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-sm-3">Radio</div>
			<div class="col-sm-9">
				<div class="custom-control custom-radio">
					<input type="radio" value="1" id="myradio1" name="myradio" class="custom-control-input"<?php echo $myradio1 ?>>
					<label class="custom-control-label" for="myradio1">INPUT Radio 1</label>
				</div>
				<div class="custom-control custom-radio">
					<input type="radio" value="2" id="myradio2" name="myradio" class="custom-control-input"<?php echo $myradio2 ?>>
					<label class="custom-control-label" for="myradio2">INPUT Radio 2</label>
				</div>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-sm-3">
				<label for="myselect">SELECT</label>
			</div>
			<div class="col-sm-8">
				<select name="myselect" id="myselect" class="form-control">
					<option value="1"<?php echo $myselect1 ?>>Option 1</option>
					<option value="2"<?php echo $myselect2 ?>>Option 2</option>
					<option value="3"<?php echo $myselect3 ?>>Option 3</option>
				</select>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-sm-3">
				<label for="mytextarea">Textarea</label>
			</div>
			<div class="col-sm-8">
				<textarea name="mytextarea" id="mytextarea" class="form-control"><?php echo $mytextarea; ?></textarea>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-sm-3">
				<label for="range">Range</label>
			</div>
			<div class="col-sm-8">
				<input type="range" class="custom-range" id="range">
			</div>
		</div>
		<div class="text-center">
			<input type="submit">
		</div>
		<?php
		
		if(isset($_POST['myemail']) && $_POST['myemail']!='' && !filter_var(trim(strtolower($_POST['myemail'])), FILTER_VALIDATE_EMAIL)) {
			echo '<div class="alert alert-danger text-center mt-1" role="alert">Die E-Mail-Adresse ist ungültig</div>';
		}
		
		?>
		<hr>
		<div class="text-center">
			<a href="Nur%20mit%20HTML.html">Nur mit HTML</a>
		</div>
	</form>
</body>
</html>
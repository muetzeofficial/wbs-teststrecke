<?php
function NHparsePrism($lang,$string,$linenums=1) {
	$lang = $lang=='html' || $lang=='HTML' ? 'markup':$lang;
	$lnum = $linenums==1 ? ' class="line-numbers"':'';
	echo '<pre'.$lnum.'><code class="language-'.strtolower($lang).'">'.htmlspecialchars($string).'</code></pre>';
}

function NHgettemplate($template) {
    return str_replace("\"","\\\"",implode("",file($template)));
}

function NHaction() {
	global $_SERVER;
	$string = explode('/',strrev($_SERVER['PHP_SELF']));
	
	return strrev($string[0]);
}
?>